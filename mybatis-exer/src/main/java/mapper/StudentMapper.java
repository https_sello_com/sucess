package mapper;

import java.util.List;
import java.util.Map;

import pojo.Students;

public interface StudentMapper {

	void insert(Students stu);

	void delete(Integer id);

	void update(Students stu);

	Students selectbyId(Integer id);

	List<Students> selectAll();

	public Map selectMap();

	public List<Map> selectListMap();

	public List<Students> selectBylike(Students stu);

//	public List<User> selectByPage(Page page);

	//public User selectById2(int id);

	//public User selectById3(int id);

}
