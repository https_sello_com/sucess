package test;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import pojo.Students;
import service.StudentService;

public class StudentTest {
	//private StudentService s=new StudentService();
	
	public StudentService s;
	
	@Before
	public void init() {	
		s=new StudentService();
	}
	
	@Test
	public void testSave(){
		Students student = new Students();
		student.setName("���");
		student.setAge(22);
		s.save(student);
		System.out.println(student.getId());
	}
	@Test
	public void testRemove(){
		s.remove(11);
	}
	
	@Test
	public void testUpdate(){
		Students stu = new Students();
		stu.setId(12);
		stu.setName("����");
		s.Update(stu);
	}
	@Test
	public void testfindStu(){
		Students stu = s.findStu(12);
		
		System.out.println(stu.getName()+stu.getAge());
	}
	
	@Test
	public void testfindStu2(){
		List<Students> list = s.findStu2();
		
		System.out.println(list);
	}
	@Test
	public void testfindStu3(){
	           List<Map> map = s.findStu3();
		
		System.out.println(map);
	}

}
